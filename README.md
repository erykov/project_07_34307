#Milionerzy
Projekt opiera się na formacie popularnego teleturnieju "milionerzy". Aby wygrać
należy udzielić poprawnej odpowiedzi na wszystkie 15 pytań. Jeśli gracz udzieli
błędnej odpowiedzi to następuje koniec gry i uzyskuje sumę pieniędzy, która była 
do wygrania w pytaniu poprzednim. 
Gracz ma do dyspozycji 3 koła ratunkowe, które może użyć jednorazowo.
* 50/50
* Pytanie do publiczności
* Telefon do przyjaciela

##Główne założenia projektu
Projekt jest aplikacja konsolową. Pytania są pobierana z 15 plików csv reprezentujących
poszczególne etapy gry. W każym z nich znajduje się 10 pytań. Wprowadza to urozmaicenie 
w rozgrywce, ponieważ podczas rozgrywki pytania są losowane. Koła ratunkowe działają w 
następujący sposób : 
* 50/50 - odrzuca 2 nieprawidłowe odpowiedzi
* Pytanie do publiczności - dla odpowiedzi prawidłowej losowana jest wartość z przedziału 50 do 100.
Od wartości 100 odejmowana jest wylosowana wartość dla odpowiedzi prawidłowej i ponownie losowana
jest kolejna wartość tym razem z przedziału od 0 do różnicy wartości.
* Telefon do przyjaciela - do pytania nr 5 przyjaciel zawsze podaje prawidłową odpowiedź. Natomiast
w kolejnych etapach jest to losowanie 1 z 4 odpowiedzi.

Jako język programowania została użyta Java. Natomiast jako środowisko zostało użyte 
Intellij firmy JetBrains. W procesie tworzenia aplikacji został również użyty proces
debugowania, który znacząco ułatwił analizę kodu.

##Podział zadań wśród zespołu 
Aleksandra Piątek
* utworzenie klasy reprezentującej pytanie
* utworzenie klasy reprezentującej odpowiedź 
* utworzenie metody reprezentującej pytanie do publiczności
* utworzenie metody odpowiadającej za wygląd menu
* testowanie poszczególnych linii kodu i ich poprawa

Eryk Marnik 
* utworzenie klasy odpowiadającej za operację na plikach csv
* utworzenie metody reprezentującej 50/50 
* utworzenie metody reprezentującej telefon do przyjaciela
* utworzenie klasy odpowiadającej za grę
* testowanie poszczególnych linii kodu i ich poprawa

##Wygląd aplikacji i funkcjonalność
Aby wybrać odpowiedź użytkownik podaje z klawiatury cyfrę od 1 do 4.
Jeśli chce skorzystać z koła ratunkowego to wprowadza liczbę z zakresu
od 5 do 7. W przypadku rezygnacji należy wcisnąć cyfrę 8. Każdy inny
znak podany z klawiatury będzie skutkował przerwaniem działania programu
i rzuceniem wyjątku.

###Skład zespołu :
-Aleksandra Piątek

-Eryk Marnik