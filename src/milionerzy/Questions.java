package milionerzy;

import java.io.IOException;

public class Questions extends FileOperations{
    private String question;
    private final String questionContent;

    Questions(String stage) throws IOException {
        super(stage);
        this.questionContent = getRecord();
        setQuestion();
    }

    private void setQuestion() {
        String[] temp = this.questionContent.split(";");
        this.question = temp[1];
    }

    public String getQuestion() {
        return this.question;
    }

    public String getQuestionContent() {
        return this.questionContent;
    }

    @Override
    public String toString() {
        return this.question;
    }

    public class Answer {
        private final String[] answers = new String[4];
        private int correctAnswer;

        Answer() {
            setAnswers();
        }

        private void setAnswers() {
            String[] temp = getRecord().split(";");
            this.correctAnswer = Integer.parseInt(temp[6]);
            int j = 0;
            for(int i = 2; i < 6; i++) {
                this.answers[j] = temp[i];
                j++;
            }
        }

        public String[] getAnswers() {
            return this.answers;
        }

        public int getCorrectAnswer() {
            return this.correctAnswer;
        }

        @Override
        public String toString() {
            return String.valueOf(this.correctAnswer);
        }
    }
}
