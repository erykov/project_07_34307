package milionerzy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

public class LifeLine {
    public static String[] fiftyFifty(Questions.Answer a) throws IOException {
        String[] answers = a.getAnswers();
        int count = 0;
        int correctAnswer = a.getCorrectAnswer();
        String emptyString = " ";
        while(count < 2) {
            int randomNum = ThreadLocalRandom.current().nextInt(0, a.getAnswers().length - 1);
            if(!Objects.equals(answers[randomNum], answers[correctAnswer - 1]) && !answers[randomNum].equals(emptyString)) {
                answers[randomNum] = emptyString;
                count++;
            }
        }
        return answers;
    }

    public static List viewers(Questions.Answer a) {
        List<Integer> percents = new ArrayList<Integer>();
        int chanceForCorrectAnswer = ThreadLocalRandom.current().nextInt(50, 100);
        percents.add(chanceForCorrectAnswer);
        int chanceForIncorrectAnswer = 100 - chanceForCorrectAnswer;
        int firstIncorrectAnswer = ThreadLocalRandom.current().nextInt(0, chanceForIncorrectAnswer);
        percents.add(firstIncorrectAnswer);
        chanceForIncorrectAnswer -= firstIncorrectAnswer;
        int secondIncorrectAnswer = ThreadLocalRandom.current().nextInt(0, chanceForIncorrectAnswer);
        percents.add(secondIncorrectAnswer);
        chanceForIncorrectAnswer -= secondIncorrectAnswer;
        int thirdIncorrectAnswer = chanceForIncorrectAnswer;
        percents.add(thirdIncorrectAnswer);
        return percents;
    }
    public static String friend(Questions question, Questions.Answer a) {
        if(question.getStage() <= 5) {
            return a.getAnswers()[a.getCorrectAnswer() - 1];
        }
        else {
            boolean isEmpty = true;
            String friendAnswer = null;
            while(isEmpty) {
                int randomNum = ThreadLocalRandom.current().nextInt(0, a.getAnswers().length - 1);
                friendAnswer = a.getAnswers()[randomNum];
                if(!Objects.equals(a.getAnswers()[randomNum], " ")) {
                    isEmpty = false;
                }
            }
            return friendAnswer;
        }
    }
}
