package milionerzy;

import java.io.IOException;

public class GameInit {
  private final Game[] game = {new Game("1.csv"), new Game("2.csv"), new Game("3.csv"), new Game("4.csv"),
      new Game("5.csv"), new Game("6.csv"), new Game("7.csv"), new Game("8.csv"),
      new Game("9.csv"), new Game("10.csv"), new Game("11.csv"), new Game("12.csv"),
      new Game("13.csv"), new Game("14.csv"), new Game("15.csv")
  };

  private final static int[] prize = {100, 200, 300, 500, 1000, 2000, 4000, 8000, 16000, 32000, 64000, 125000, 250000,
          500000, 1000000
  };

  GameInit() throws IOException {}

  public void startGame() throws IOException, WrongData {
    int i = 0;
    while(i < 15) {
      System.out.println("wygrana: " + prize[i] + " zl");
      game[i].menu();
      game[i].questions();
      if(!game[i].getContinuing()) {
        if(i == 0) {
          System.out.println("wygrales : 0 zl");
        }
        else {
          System.out.println("wygrales : " + prize[i-1] + " zl");
        }
        break;
      }
      i++;
    }
    if(i == 14 && game[i].getContinuing()) {
      System.out.println("Brawo wygrales !! Jestes milionerem !!!");
    }
  }

}
