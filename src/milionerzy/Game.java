package milionerzy;

import java.io.IOException;
import java.util.Objects;
import java.util.Scanner;

import static milionerzy.LifeLine.fiftyFifty;
import static milionerzy.LifeLine.friend;
import static milionerzy.LifeLine.viewers;

public class Game {
  private static boolean continuing;
  private static boolean wasFiftyFiftyUsed;
  private static boolean wasFriendTelephoneUsed;
  private static boolean wasViewersHelpUsed;
  private String answerA;
  private String answerB;
  private String answerC;
  private String answerD;
  private final Questions question;
  private final Questions.Answer answers;

  Game(String stage) throws IOException {
    this.question = new Questions(stage);
    this.answers = question.new Answer();
    this.answerA = answers.getAnswers()[0];
    this.answerB = answers.getAnswers()[1];
    this.answerC = answers.getAnswers()[2];
    this.answerD = answers.getAnswers()[3];
  }

  public void menu() {
    System.out.println("Etap : " + question.getStage());
    System.out.println(question.getQuestion());
    System.out.println("1 : " + this.answerA + "\n2 : " + this.answerB + "\n3 : " + this.answerC + "\n4 : " + this.answerD);
    System.out.println("5 : 50/50" + "\n6 : Pytanie do publicznosci" + "\n7 : Telefon do przyjaciela" + "\n8 : Rezygnacja");
  }

  public void checkAnswer(int number) {
    continuing = Objects.equals(answers.getAnswers()[number - 1], answers.getAnswers()[answers.getCorrectAnswer() - 1]);
    if (!continuing) {
      System.out.println("Bledna odpowiedz, poprawna to : " + answers.getAnswers()[answers.getCorrectAnswer() - 1]);
    }
  }

  public void questions() throws IOException, WrongData {
    int answer;
    boolean isGivenAnswer = false;
    Scanner scan = new Scanner(System.in);
    while (!isGivenAnswer) {
      System.out.println("Odpowiedz : ");
      answer = scan.nextInt();
      switch (answer) {
        case 1, 2, 3, 4:
          checkAnswer(answer);
          isGivenAnswer = true;
          break;
        case 5:
          if (wasFiftyFiftyUsed) {
            System.out.println("Nie mozna ponownie uzyc");
          } else {
            wasFiftyFiftyUsed = true;
            String[] afterFiftyFiftyAnswers = fiftyFifty(this.answers);
            this.answerA = afterFiftyFiftyAnswers[0];
            this.answerB = afterFiftyFiftyAnswers[1];
            this.answerC = afterFiftyFiftyAnswers[2];
            this.answerD = afterFiftyFiftyAnswers[3];
            System.out.println("1 : " + this.answerA + "\n2 : " + this.answerB + "\n3 : " + this.answerC + "\n4 : " + this.answerD);
          }
          break;
        case 6:
          if (wasViewersHelpUsed) {
            System.out.println("Nie mozna ponownie uzyc");
          } else {
            afterViewersHelp();
            wasViewersHelpUsed = true;
          }
          break;
        case 7:
          if (wasFriendTelephoneUsed) {
            System.out.println("Nie mozna ponownie uzyc");
          } else {
            System.out.println("Wydaje mi sie, ze poprawna odpowiedz to : " + friend(this.question, this.answers));
            wasFriendTelephoneUsed = true;
          }
          break;
        case 8:
          isGivenAnswer = true;
          continuing = false;
          break;
        default:
          throw new WrongData("Operacja nie jest dozwolona");
      }
    }
  }

  public boolean getContinuing() {
    return continuing;
  }

  public void afterViewersHelp() {
    int correct = this.answers.getCorrectAnswer() - 1;
    if(correct == 0) {
      System.out.println("1 : " + this.answerA + " " + viewers(this.answers).get(0) + "%");
      System.out.println("2 : " + this.answerB + " " + viewers(this.answers).get(1) + "%");
      System.out.println("3 : " + this.answerC + " " + viewers(this.answers).get(2) + "%");
      System.out.println("4 : " + this.answerD + " " + viewers(this.answers).get(3) + "%");
    }
    else if(correct == 1) {
      System.out.println("1 : " + this.answerA + " " + viewers(this.answers).get(1) + "%");
      System.out.println("2 : " + this.answerB + " " + viewers(this.answers).get(0) + "%");
      System.out.println("3 : " + this.answerC + " " + viewers(this.answers).get(2) + "%");
      System.out.println("4 : " + this.answerD + " " + viewers(this.answers).get(3) + "%");
    }
    else if(correct == 2) {
      System.out.println("1 : " + this.answerA + " " + viewers(this.answers).get(2) + "%");
      System.out.println("2 : " + this.answerB + " " + viewers(this.answers).get(1) + "%");
      System.out.println("3 : " + this.answerC + " " + viewers(this.answers).get(0) + "%");
      System.out.println("4 : " + this.answerD + " " + viewers(this.answers).get(3) + "%");
    }
    else {
      System.out.println("1 : " + this.answerA + " " + viewers(this.answers).get(3) + "%");
      System.out.println("2 : " + this.answerB + " " + viewers(this.answers).get(1) + "%");
      System.out.println("3 : " + this.answerC + " " + viewers(this.answers).get(2) + "%");
      System.out.println("4 : " + this.answerD + " " + viewers(this.answers).get(0) + "%");
    }
  }
}

