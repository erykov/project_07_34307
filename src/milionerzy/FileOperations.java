package milionerzy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class FileOperations {
  private List<String> records = new ArrayList<>();
  private final String fileName;
  private final String record;

  FileOperations(String fileName) throws IOException {
    this.records = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
    this.fileName = fileName;
    this.record = drawQuestion();
  }

  public String getRecord() {
    return this.record;
  }

  public int getStage() {
    String[] stage = this.fileName.split("\\.");
    return Integer.parseInt(stage[0]);
  }

  public String getFileName() {
    return this.fileName;
  }

  public String drawQuestion() {
    int randomNum = ThreadLocalRandom.current().nextInt(1, 10 + 1);
    return this.records.get(randomNum);
  }


}
